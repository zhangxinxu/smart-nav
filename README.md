# smart-nav

#### 介绍
借助IntersectionObserver API实现文档标题的自动导航能力，带实验性质，不适合在正式项目中使用。

#### 演示

戳这里：https://www.zhangxinxu.com/study/202011/intersection-observer-title-nav.html

#### 使用说明

引入JS文件：
```html
<script src="./smart-nav.js"></script>
```

调用：
```js
smartNav('article h3');
```

会把符合选择器`'article h3'`的元素聚合成和快速定位的导航元素。

#### 语法
```js
smartNav(elements, options);
```

其中：

<dl>
    <dt>elements</dt>
    <dd>必需。标题元素们，可以是Nodelist对象，也可以是元素的选择器字符串。</dd>
    <dt>options</dt>
    <dd>可选。目前支持一个参数如下表：</dd>
    <table>
<thead>
<tr>
<th>API名称</th>
<th>默认值</th>
<th>释义</th>
</tr>
</thead>
<tbody>
<tr>
<td>nav</td>
<td>null</td>
<td>导航容器对象，创建的导航列表会append到这里。如果为null，本JS会自动创建一个容器元素，结构为：div.title-nav-ul &gt; a.title-nav-li</td>
</tr>
</tbody>
</table>
</dl>


#### 许可证

MIT
